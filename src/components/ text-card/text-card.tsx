import { FC } from "react";
import styles from "./text-card.module.scss";

const TextCard: FC<{ cardTitle?: string; content: string }> = (props) => {
  return (
    <div className={styles["card"]}>
      <h3 className={styles["card-title"]}>{props.cardTitle}</h3>
      <p className={styles["smaller-text"]}>{props.content}</p>
    </div>
  );
};

export default TextCard;
