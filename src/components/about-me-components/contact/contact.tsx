import { FC } from "react";
import texts from "../../../consts/text.json";
import styles from "./contac.module.scss";

const ContactComponent: FC = () => {
  const renderListItems = (list: string[]) => {
    const listItems = [];
    for (let text of list) {
      listItems.push(
        <li className={styles["list-item"]} key={text}>
          {text}
        </li>
      );
    }
    return listItems;
  };

  return (
    <div>
      <h3 className={styles["title"]}>
        {texts.generalComponents.contactComponent.title_1}
      </h3>
      <ul>
        {renderListItems(texts.generalComponents.contactComponent.array_1)}
      </ul>
      <h3 className={styles["title"]}>
        {texts.generalComponents.contactComponent.title_2}
      </h3>
      <ul>
        {renderListItems(texts.generalComponents.contactComponent.array_2)}
      </ul>
    </div>
  );
};

export default ContactComponent;
