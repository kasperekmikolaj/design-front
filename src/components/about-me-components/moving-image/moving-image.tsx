import { FC, useEffect, useState } from "react";
import personImage from "../../../assets/about-me-images/person.png";
import "../../../assets/fonts/Muli.ttf";
import texts from "../../../consts/text.json";
import styles from "./moving-image.module.scss";

const MovingImage: FC = () => {
  const imageScrollingDivider = 2;

  const [imageOffset, setImageOffset] = useState(0); // for scrolling

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  const handleScroll = () => {
    let scrollY = window.scrollY;
    setImageOffset(scrollY / imageScrollingDivider);
  };

  return (
    <section className={styles["secion-elem"]}>
      <div
        className={styles["moving-image"]}
        style={{
          transform: `translateY(${imageOffset}px)`,
        }}
      />
      <div className={styles["person-details-container"]}>
        <div className={styles["person-details-text"]}>
          <p className={styles["sub-1"]}>
            {texts.aboutMe.movingImage.subTitle_1}
          </p>
          <h1>{texts.aboutMe.movingImage.title}</h1>
          <h3 className={styles["sub-2"]}>
            {texts.aboutMe.movingImage.subTitle_2}
          </h3>
        </div>
        <img className={styles["person-image"]} src={personImage} />
      </div>
    </section>
  );
};

export default MovingImage;
