import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/react-splide/css";
import { FC } from "react";
import { aboutMe } from "../../../consts/text.json";
import styles from "./opinions.module.scss";

const Opinions: FC = () => {
  const createSlides = () => {
    const slides = [];
    for (let item of aboutMe.opinions.list) {
      slides.push(
        <SplideSlide key={item.text}>
          <div className={styles["slide"]}>
            <div className={styles["text-container"]}>
              <p>{aboutMe.opinions.title}</p>
              <p>{item.text}</p>
              <p>{item.author}</p>
            </div>
          </div>
        </SplideSlide>
      );
    }
    return slides;
  };

  return (
    <div className={styles["container"]}>
      <Splide
        options={{
          autoplay: true,
          interval: 5000,
          pauseOnHover: true,
          rewind: true,
          speed: 900,
        }}
      >
        {createSlides()}
      </Splide>
    </div>
  );
};

export default Opinions;
