import { ChangeEvent, FC, FormEvent } from "react";
import { contact_page } from "../../consts/text.json";
import useInput from "../../hooks/input-hook";
import styles from "./contact-form.module.scss";

const ContactForm: FC = () => {
  const texts = contact_page.form_component;

  const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
  const validateEmail = (email: string) => emailRegex.test(email.trim());
  const {
    value: emailValue,
    setValue: setEmail,
    setIsTouched: setIsEmailTouched,
    showError: showEmailError,
  } = useInput(validateEmail);

  const validateBasicValue = (message: string) => message.trim() !== "";
  const {
    value: messageValue,
    setValue: setMessage,
    setIsTouched: setIsMessageTouched,
    showError: showMessageError,
  } = useInput(validateBasicValue);

  const {
    value: nameValue,
    setValue: setName,
    setIsTouched: setIsNameTouched,
    showError: showNameError,
  } = useInput(validateBasicValue);

  const showFormError = showEmailError || showNameError || showMessageError;
  const isFormValid =
    validateEmail(emailValue) &&
    validateBasicValue(messageValue) &&
    validateBasicValue(nameValue);

  const handleFormSubmit = (event: FormEvent) => {
    event.preventDefault();
    //todo
  };

  return (
    <div className={styles["form-container"]}>
      <form onSubmit={handleFormSubmit}>
        <div className={styles["name-mail-block"]}>
          <div className={styles["group"]}>
            {/* name input */}
            <input
              onChange={(event: ChangeEvent<HTMLInputElement>) =>
                setName(event.target.value)
              }
              onBlur={() => setIsNameTouched(true)}
              className={`${showNameError ? styles["invalid"] : ""}  ${
                styles["input"]
              }`}
            />
            <span className={styles["bar"]}></span>
            <label className={styles["label"]}>{texts.name}</label>
          </div>

          {/* mail input */}
          <div className={styles["group"]}>
            <input
              type="text"
              onChange={(event: ChangeEvent<HTMLInputElement>) =>
                setEmail(event.target.value)
              }
              onBlur={() => setIsEmailTouched(true)}
              className={`${showEmailError ? styles["invalid"] : ""}  ${
                styles["input"]
              }`}
            />
            <span className={styles["bar"]}></span>
            <label className={styles["label"]}>{texts.emali}</label>
          </div>
        </div>
        {/* text area */}
        <div className={styles["text-area-container"]}>
          <div className={styles["group"]}>
            <textarea
              rows={6}
              onChange={(event: ChangeEvent<HTMLTextAreaElement>) =>
                setMessage(event.target.value)
              }
              onBlur={() => setIsMessageTouched(true)}
              className={`${showMessageError ? styles["invalid"] : ""}  ${
                styles["input"]
              }`}
            />
            <span className={styles["bar"]}></span>
            <label className={styles["label"]}>{texts.content}</label>
            {showFormError && (
              <p className={styles["invalid-form-info"]}>
                {texts.form_invalid_info}
              </p>
            )}
          </div>
        </div>
        <button
          type="submit"
          className={`${
            isFormValid
              ? styles["button"] + " " + styles["valid"]
              : styles["button"]
          }`}
        >
          {texts.button_text}
        </button>
      </form>
    </div>
  );
};

export default ContactForm;
