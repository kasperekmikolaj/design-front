import { FC } from "react";
import { footer } from "../../consts/text.json";
import SocialMediaIconsList from "../general/social-media-icons/social-media-icons";
import styles from "./footer.module.scss";

const Footer: FC = () => {
  return (
    <>
      <div className={styles["pre-footer-background"]}>
        <div className={styles["pre-footer"]}>
          <h1>{footer.pre_footer}</h1>
        </div>
      </div>
      <div className={styles["background"]}>
        <div className={styles["container"]}>
          <div className={styles["section"]}>
            <h4>{footer.contact_section.title}</h4>
            <p>{footer.contact_section.company_name}</p>
            <p>{footer.contact_section.name}</p>
            <p>e-mail:</p>
            <p>{footer.contact_section.email}</p>
            <p>tel. {footer.contact_section.telephone}</p>
          </div>
          <div className={styles["section"]}>
            <h4>{footer.privacy_policy_section.title}</h4>
            <a href={footer.privacy_policy_section.link}>
              <p>{footer.privacy_policy_section.text}</p>
            </a>
          </div>
          <div className={styles["section"]}>
            <h4>{footer.additional_section.title}</h4>
            <div style={{ display: "inline-block" }}>
              <SocialMediaIconsList />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
