import { FC, useState } from "react";
import styles from "./collapse.module.scss";

const Collapse: FC<{ title: string; text: string }> = ({ title, text }) => {
  const [showText, setShowText] = useState(false);

  const onTitleClick = () => {
    setShowText((oldVal) => {
      return !oldVal;
    });
  };

  return (
    <>
      <div className={styles["title-container"]} onClick={onTitleClick}>
        <h4 className={styles["title"]}>{title}</h4>
        <div className={styles["icon-container"]}>
          <h4 className={styles["icon"]}> {showText ? "-" : "+"}</h4>
        </div>
      </div>
      <p
        className={styles["content"]}
        style={{ display: `${showText ? "block" : "none"}` }}
      >
        {text}
      </p>
    </>
  );
};

export default Collapse;
