import { FC, useState } from "react";
import ReactImageGallery, { ReactImageGalleryItem } from "react-image-gallery";
import "react-image-gallery/styles/scss/image-gallery.scss";
import imageNames from "../../../assets/portfolio-images/gallery-filenames.json";
import styles from "./gallery.module.scss";

const Gallery: FC = () => {
  const [imageClass, setImageClass] = useState(styles["custom-image"]);
  const images = imageNames.imageNames;

  const handleFullScreen = (isFullscreen: boolean) => {
    isFullscreen
      ? setImageClass(styles["fullscreen-image"])
      : setImageClass(styles["custom-image"]);
  };

  const getImages = () => {
    const list: ReactImageGalleryItem[] = [];
    for (let image of images) {
      const imagePath = "/" + image;
      list.push({
        original: imagePath,
        originalClass: imageClass,
        thumbnail: imagePath,
      });
    }

    return list;
  };

  return (
    <div className={styles["container"]}>
      <ReactImageGallery
        items={getImages()}
        thumbnailPosition="left"
        autoPlay
        onScreenChange={handleFullScreen}
      />
    </div>
  );
};

export default Gallery;
