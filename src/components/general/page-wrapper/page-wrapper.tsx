import { FC, PropsWithChildren } from "react";
import styles from "./page-wrapper.module.scss";

const PageWrapper: FC<PropsWithChildren> = (props) => {
  return <div className={styles["content-div"]}>{props.children}</div>;
};

export default PageWrapper;
