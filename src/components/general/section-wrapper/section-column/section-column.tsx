import { FC, PropsWithChildren } from "react";
import styles from "./section-column.module.scss";

const SectionColumn: FC<PropsWithChildren<{ className?: string }>> = (
  props
) => {
  return (
    <div className={`${styles["section-column"]} ${props.className}`}>
      {props.children}
    </div>
  );
};

export default SectionColumn;
