import { FC, PropsWithChildren } from "react";
import styles from "./section-wrapper.module.scss";

const SectionWrapper: FC<
  PropsWithChildren<{
    borderLeft?: boolean;
    borderRight?: boolean;
    className?: string;
  }>
> = (props) => {
  const borderleftWidth = props.borderLeft ? "0.3rem" : "0";
  const borderRightWidth = props.borderRight ? "0.3rem" : "0";
  return (
    <section
      className={`${styles["section-elem"]} ${props.className}`}
      style={{
        borderLeftWidth: borderleftWidth,
        borderRightWidth: borderRightWidth,
      }}
    >
      {props.children}
    </section>
  );
};

export default SectionWrapper;
