import { FC } from "react";
import styles from "./social-media-icon.module.scss";

const SocialMediaIcon: FC<{ icon: string; href: string }> = (props) => {
  return (
    <a className={styles["link"]} href={props.href}>
      <div>
        <img src={props.icon} className={styles["icon"]} />
      </div>
    </a>
  );
};

export default SocialMediaIcon;
