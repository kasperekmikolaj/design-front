import { FC } from "react";
import fbIcon from "../../../assets/social-media-icons/facebook.png";
import instagramIcon from "../../../assets/social-media-icons/instagram.png";
import linkedInIcon from "../../../assets/social-media-icons/linkedin.svg";
import SocialMediaIcon from "../social-media-icon/social-media-icon";
import styles from "./social-media-icons.module.scss";

const SocialMediaIconsList: FC = () => {
  return (
    <div className={styles["social-media-icons-list"]}>
      <SocialMediaIcon icon={fbIcon} href="" />
      <SocialMediaIcon icon={linkedInIcon} href="" />
      <SocialMediaIcon icon={instagramIcon} href="" />
    </div>
  );
};

export default SocialMediaIconsList;
