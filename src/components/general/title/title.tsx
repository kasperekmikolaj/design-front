import { FC } from "react";
import styles from "./title.module.scss";

const Title: FC<{ subtitle?: string; title: string; className?: string }> = (
  props
) => {
  return (
    <div className={`${styles["container"]} ${props.className}`}>
      <h4 className={styles["subtitle"]}>{props.subtitle}</h4>
      <h2 className={styles["title"]}>{props.title}</h2>
    </div>
  );
};

export default Title;
