import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import { RouteSpec, routeSpecs } from "../../../consts/consts-and-names";
import { navigationActions } from "../../../store/react-store";
import NavLinkWrapper from "../nav-link-wrapper";
import styles from "./mobile-navigation.module.scss";

const MobileNavigation = React.memo(() => {
  const dispatch = useDispatch();

  const activeRouteHandler = useCallback((isActive: boolean): string => {
    return `${styles.link} ${isActive ? styles.activeLink : ""}`;
  }, []);

  return (
    <div
      onClick={() => dispatch(navigationActions.changeMobileNavVisibility())}
      className={styles["mobile-navigation-container"]}
    >
      <div className={styles.backdrop}></div>
      <div className={styles["nav-list"]}>
        <div className={styles["links-container"]}>
          {Object.entries(routeSpecs).map(
            ([routeName, routeSpec]: [string, RouteSpec]) => (
              <NavLinkWrapper
                matchEnd={routeSpec.path === "/"}
                routeSpec={routeSpec}
                key={routeName}
                activeRouteHandler={activeRouteHandler}
              ></NavLinkWrapper>
            )
          )}
        </div>
      </div>
    </div>
  );
});

export default MobileNavigation;
