import React from "react";
import { NavLink } from "react-router-dom";
import { RouteSpec } from "../../consts/consts-and-names";

const NavLinkWrapper: React.FC<{
  routeSpec: RouteSpec;
  activeRouteHandler: (isActive: boolean) => string;
  matchEnd: boolean;
}> = React.memo((props) => {
  return (
    <NavLink
      to={props.routeSpec.path}
      className={({ isActive }) => props.activeRouteHandler(isActive)}
      end={props.matchEnd}
    >
      {props.routeSpec.text}
    </NavLink>
  );
});

export default NavLinkWrapper;
