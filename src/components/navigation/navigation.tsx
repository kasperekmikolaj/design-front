import { useCallback } from "react";
import { useDispatch } from "react-redux";
import iconImage from "../../assets/icon.webp";
import { RouteSpec, routeSpecs } from "../../consts/consts-and-names";
import { navigationActions } from "../../store/react-store";
import NavLinkWrapper from "./nav-link-wrapper";
import styles from "./navigation.module.scss";

const NavigationComponent = () => {
  const dispatch = useDispatch();

  const activeRouteHandler = useCallback((isActive: boolean): string => {
    return `${styles.link} ${isActive ? styles.activeLink : ""}`;
  }, []);

  const mobileNavButtonHandler = () => {
    dispatch(navigationActions.changeMobileNavVisibility());
  };

  return (
    <nav>
      <div className={styles["navigation-container"]}>
        <img className={styles.icon} src={iconImage} />
        <button
          type="button"
          className={styles["toggle-button"]}
          onClick={mobileNavButtonHandler}
        >
          <span className={styles["toggle-button__bar"]}></span>
          <span className={styles["toggle-button__bar"]}></span>
          <span className={styles["toggle-button__bar"]}></span>
        </button>
        <div className={styles["navigation-items"]}>
          {Object.entries(routeSpecs).map(
            ([routeName, routeSpec]: [string, RouteSpec]) => (
              <NavLinkWrapper
                routeSpec={routeSpec}
                activeRouteHandler={activeRouteHandler}
                matchEnd={routeSpec.path === "/"}
                key={routeName}
              />
            )
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavigationComponent;
