export type RouteSpec = { path: string; text: string };

export const routeSpecs = {
  aboutMe: { path: "/", text: "O mnie" },
  portfolio: { path: "/portfolio", text: "Portfolio" },
  ofer: { path: "/ofer", text: "Oferta" },
  prices: { path: "/prices", text: "Cennink" },
  contact: { path: "/contact", text: "Kontakt" },
};
