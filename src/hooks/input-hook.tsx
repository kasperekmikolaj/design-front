import { useState } from "react";

const useInput = (validateValue: (value: string) => boolean) => {
  const [value, setValue] = useState("");
  const [isTouched, setIsTouched] = useState(false);
  const showError = !validateValue(value) && isTouched;

  return { value, setValue, setIsTouched, showError };
};

export default useInput;
