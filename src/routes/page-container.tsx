import { useSelector } from "react-redux";
import { Outlet } from "react-router-dom";
import Footer from "../components/footer/footer";
import MobileNavigation from "../components/navigation/mobile-navigation/mobile-navigation";
import NavigationComponent from "../components/navigation/navigation";
import { RootStoreState } from "../store/react-store";

const PageContainer = () => {
  const showMobileNav = useSelector(
    (state: RootStoreState) => state.nav.showMobile
  );

  return (
    <>
      {showMobileNav && <MobileNavigation />}
      <div style={{ marginBottom: "3.2rem" }}>
        <NavigationComponent />
      </div>
      <Outlet />
      <Footer />
    </>
  );
};

export default PageContainer;
