import { FC } from "react";
import s1_im1 from "../../../assets/about-me-images/s1_im_1.jpg";
import s1_im2 from "../../../assets/about-me-images/s1_im_2.jpg";
import s2_im1 from "../../../assets/about-me-images/s2_im_1.jpg";
import s2_im2 from "../../../assets/about-me-images/s2_im_2.jpg";
import s3_im1 from "../../../assets/about-me-images/s3_im1.jpg";
import s3_im2 from "../../../assets/about-me-images/s3_im2.jpg";
import TextCard from "../../../components/ text-card/text-card";
import ContactComponent from "../../../components/about-me-components/contact/contact";
import MovingImage from "../../../components/about-me-components/moving-image/moving-image";
import Opinions from "../../../components/about-me-components/opinions/opinions";
import PageWrapper from "../../../components/general/page-wrapper/page-wrapper";
import SectionColumn from "../../../components/general/section-wrapper/section-column/section-column";
import SectionWrapper from "../../../components/general/section-wrapper/section-wrapper";
import SocialMediaIconsList from "../../../components/general/social-media-icons/social-media-icons";
import Title from "../../../components/general/title/title";
import texts from "../../../consts/text.json";
import styles from "./about-me.module.scss";

const AboutMePage: FC = () => {
  return (
    <>
      <MovingImage />
      <PageWrapper>
        {/* section 1 */}
        <SectionWrapper borderRight className={styles["first-section"]}>
          <SectionColumn>
            <Title
              title={texts.aboutMe.section_1.title}
              subtitle={texts.aboutMe.section_1.subTitle}
            ></Title>
            <p className={styles["normal-text"]}>
              {texts.aboutMe.section_1.text_1}
            </p>
            <img
              src={s1_im1}
              className={`${styles["responsive-image"]} ${styles["first-section_image-1"]}`}
            />
          </SectionColumn>
          <SectionColumn>
            <img
              src={s1_im2}
              className={`${styles["responsive-image"]} ${styles["circle-image"]}`}
            />
            <div style={{ width: "75%" }}>
              <p className={styles["smaller-text"]}>
                {texts.aboutMe.section_1.text_2}
              </p>
            </div>
            <SocialMediaIconsList />
          </SectionColumn>
        </SectionWrapper>
        {/* section 2 */}
        <SectionWrapper>
          <Title
            title={texts.aboutMe.section_2.title}
            className={styles["full-title"]}
          />
          <SectionColumn>
            <TextCard
              cardTitle={texts.aboutMe.section_2.card_1_title}
              content={texts.aboutMe.section_2.card_1}
            ></TextCard>
            <img src={s2_im1} className={styles["responsive-image"]} />
          </SectionColumn>
          <SectionColumn>
            <img src={s2_im2} className={styles["responsive-image"]} />
            <TextCard
              cardTitle={texts.aboutMe.section_2.card_2_title}
              content={texts.aboutMe.section_2.card_2}
            ></TextCard>
          </SectionColumn>
        </SectionWrapper>
        {/* section 3 */}
        <SectionWrapper>
          <Title
            title={texts.aboutMe.section_3.title}
            className={styles["full-title"]}
          />
          <SectionColumn>
            <TextCard
              cardTitle={texts.aboutMe.section_3.card_1_title}
              content={texts.aboutMe.section_2.card_1}
            ></TextCard>
            <img src={s3_im1} className={styles["responsive-image"]} />
          </SectionColumn>
          <SectionColumn>
            <img src={s3_im2} className={styles["responsive-image"]} />
            <TextCard
              cardTitle={texts.aboutMe.section_3.card_2_title}
              content={texts.aboutMe.section_3.card_2}
            ></TextCard>
          </SectionColumn>
        </SectionWrapper>
        {/* section 4 */}
        <SectionWrapper>
          <SectionColumn>
            <Title
              title={texts.aboutMe.section_4.title}
              subtitle={texts.aboutMe.section_4.subTitle}
            />
            <p className={styles["normal-text"]}>
              {texts.aboutMe.section_4.text_1}
            </p>
          </SectionColumn>
          <SectionColumn>
            <ContactComponent />
            <SocialMediaIconsList />
          </SectionColumn>
        </SectionWrapper>
        <SectionWrapper>
          <Opinions />
        </SectionWrapper>
      </PageWrapper>
    </>
  );
};

export default AboutMePage;
