import ContactForm from "../../../components/contact-form/contact-form";
import PageWrapper from "../../../components/general/page-wrapper/page-wrapper";
import SectionColumn from "../../../components/general/section-wrapper/section-column/section-column";
import SectionWrapper from "../../../components/general/section-wrapper/section-wrapper";
import Title from "../../../components/general/title/title";
import { contact_page as texts } from "../../../consts/text.json";
import styles from "./contact.module.scss";

const ContactPage = () => {
  return (
    <PageWrapper>
      <Title
        title={texts.title}
        subtitle={texts.subtitle}
        className={styles["page-title"]}
      />
      <SectionWrapper>
        <SectionColumn>
          <div className={styles["text-pair"]}>
            <h1>{texts.contact_section.title}</h1>
            <p>{texts.contact_section.subtitle}</p>
          </div>
          <div className={styles["text-pair"]}>
            <h1>Telefon</h1>
            <p>{texts.contact_section.telephone}</p>
          </div>
          <div className={styles["text-pair"]}>
            <h1>E-mail</h1>
            <p>{texts.contact_section.email}</p>
          </div>
        </SectionColumn>
        <SectionColumn>
          <ContactForm />
        </SectionColumn>
      </SectionWrapper>
    </PageWrapper>
  );
};

export default ContactPage;
