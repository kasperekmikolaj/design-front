import im1 from "../../../assets/ofer-page/1.jpg";
import im2 from "../../../assets/ofer-page/2.jpg";
import im3 from "../../../assets/ofer-page/3.jpg";
import im4 from "../../../assets/ofer-page/4.jpg";
import im5 from "../../../assets/ofer-page/5.jpg";
import Collapse from "../../../components/general/collapse/collapse";
import PageWrapper from "../../../components/general/page-wrapper/page-wrapper";
import SectionColumn from "../../../components/general/section-wrapper/section-column/section-column";
import SectionWrapper from "../../../components/general/section-wrapper/section-wrapper";
import Title from "../../../components/general/title/title";
import text from "../../../consts/text.json";
import styles from "./ofer.module.scss";

const OferPage = () => {
  const pageTexts = text.ofer_page;
  const images = [im1, im2, im3, im4, im5];
  const createBulletPoints = (list: { title: string; text: string }[]) => {
    const collapses = [];
    for (const elem of list) {
      collapses.push(
        <Collapse key={elem.title} title={elem.title} text={elem.text} />
      );
    }
    return collapses;
  };

  const createSections = () => {
    const sections = [];
    let imageIndex = 0;
    for (const section of Object.values(pageTexts)) {
      sections.push(
        <SectionWrapper
          key={section.title}
          className={styles["section-wrapper"]}
        >
          {imageIndex % 2 == 0 ? (
            <>
              {createTextColumn(section)}
              {createImageColumn(images[imageIndex])}
            </>
          ) : (
            <>
              {createImageColumn(images[imageIndex])}
              {createTextColumn(section)}
            </>
          )}
        </SectionWrapper>
      );
      imageIndex++;
    }
    return sections;
  };

  const createTextColumn = (section: any) => {
    return (
      <SectionColumn>
        <Title
          className={styles["title"]}
          subtitle={section.subtitle}
          title={section.title}
        />
        <p className={styles["text"]}>{section.text}</p>
        {createBulletPoints(section.bullet_points)}
      </SectionColumn>
    );
  };
  const createImageColumn = (image: string) => {
    return (
      <SectionColumn>
        <div className={styles["image-container"]}>
          <img className={styles["responsive-image"]} src={image} />
        </div>
      </SectionColumn>
    );
  };

  return <PageWrapper>{createSections()}</PageWrapper>;
};

export default OferPage;
