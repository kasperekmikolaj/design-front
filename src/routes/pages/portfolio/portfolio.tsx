import { Link } from "react-router-dom";
import im_s2_s1 from "../../../assets/portfolio-images/s_2_im_1.jpg";
import im_s2_s2 from "../../../assets/portfolio-images/s_2_im_2.jpg";
import Gallery from "../../../components/general/gallery/gallery";
import PageWrapper from "../../../components/general/page-wrapper/page-wrapper";
import SectionColumn from "../../../components/general/section-wrapper/section-column/section-column";
import SectionWrapper from "../../../components/general/section-wrapper/section-wrapper";
import Title from "../../../components/general/title/title";
import YoutubeEmbed from "../../../components/general/yt-video/yt-video";
import { routeSpecs } from "../../../consts/consts-and-names";
import { portfolio as texts } from "../../../consts/text.json";
import styles from "./portfolio.module.scss";

const PortfolioPage = () => {
  const getTitles = () => {
    let text = "";
    for (let title of texts.section_1.job_list) {
      text = text.concat(" | ", title);
    }
    text = text.slice(2);
    return text;
  };

  const getCourses = () => {
    const elements = [];
    for (let course of texts.section_2.subsection_2.course_list) {
      elements.push(createCourseElem(course));
    }
    return elements;
  };

  const createCourseElem = (courseData: {
    title: string;
    author: string;
    link: string;
    text: string;
  }) => {
    return (
      <div className={styles["course-container"]} key={courseData.title}>
        <a className={styles["course-element"]} href={courseData.link}>
          <h4>{courseData.title}</h4>
          <p>{courseData.author}</p>
          <p>{courseData.text}</p>
        </a>
        <hr />
      </div>
    );
  };

  return (
    <PageWrapper>
      <SectionWrapper>
        <SectionColumn className={styles["s1-col1"]}>
          <div className={styles["person-image"]}></div>
        </SectionColumn>
        {/* <SectionColumn className={styles["s1-col2"]}> */}
        <SectionColumn>
          <h1 className={styles["person-name"]}>{texts.section_1.name}</h1>
          <h1 className={styles["titles"]}>{getTitles()}</h1>
          <p className={styles["text"]}>{texts.section_1.text}</p>
          <Link to={routeSpecs.ofer.path} className={styles["link"]}>
            <div className={styles["check-button"]}>
              {texts.section_1.check_button}
            </div>
          </Link>
        </SectionColumn>
      </SectionWrapper>
      {/*  */}
      <SectionWrapper borderLeft={true}>
        <SectionColumn>
          <Title
            className={styles["subsection-title"]}
            title={texts.section_2.subsection_1.title}
            subtitle={texts.section_2.subsection_1.subtitle}
          />
          <p className={styles["text"]}>{texts.section_2.subsection_1.text}</p>
        </SectionColumn>
        <SectionColumn className={styles["s2-col2"]}>
          <img
            className={`${styles["responsive-image"]} ${styles["section-image"]}`}
            src={im_s2_s1}
          />
        </SectionColumn>
      </SectionWrapper>
      {/*  */}
      <SectionWrapper borderRight={true}>
        <SectionColumn>
          <Title
            className={styles["subsection-title"]}
            title={texts.section_2.subsection_2.title}
            subtitle={texts.section_2.subsection_2.subtitle}
          />
          {getCourses()}
        </SectionColumn>
        <SectionColumn className={styles["s2-col2"]}>
          <img
            className={`${styles["responsive-image"]} ${styles["section-image"]}`}
            src={im_s2_s2}
          />
        </SectionColumn>
      </SectionWrapper>
      <SectionWrapper>
        <div className={styles["video"]}>
          <YoutubeEmbed embedId="wKuKfEM1gdo" />
        </div>
      </SectionWrapper>
      {/* gallery */}
      <SectionWrapper>
        <div className={styles["gallery-container"]}>
          <Title
            className={styles["gallery-title"]}
            title={texts.section_2.galery.title}
          />
          <Gallery />
        </div>
      </SectionWrapper>
    </PageWrapper>
  );
};

export default PortfolioPage;
