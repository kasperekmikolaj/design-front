import { FC, useState } from "react";
import styles from "./price-element.module.scss";

const PriceElement: FC<{
  title: string;
  subtitle: string;
  description: string;
  price: number;
  image: string;
  className: string;
}> = ({ title, subtitle, description, price, image, className }) => {
  const [showDescription, setShowDescription] = useState(false);

  const handleMouseOut = () => {
    setShowDescription(false);
  };

  const handleMouseEnter = () => {
    setShowDescription(true);
  };

  return (
    <div className={className}>
      <div className={styles["image-container"]}>
        <p
          style={{ visibility: !showDescription ? "hidden" : "visible" }}
          onMouseOut={handleMouseOut}
        >
          {description}
        </p>
        <img
          style={{ visibility: showDescription ? "hidden" : "visible" }}
          onMouseOver={handleMouseEnter}
          src={image}
        />
      </div>

      {/* <h4>{subtitle}</h4> */}
      <div className={styles["texts"]}>
        <h3>{title}</h3>
        <h3 className={styles["price"]}>{price},00zł</h3>
      </div>
    </div>
  );
};

export default PriceElement;
