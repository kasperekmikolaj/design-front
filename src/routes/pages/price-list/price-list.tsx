import { FC } from "react";
import { Link } from "react-router-dom";
import images from "../../../assets/prices-page/prices_image_names.json";
import PageWrapper from "../../../components/general/page-wrapper/page-wrapper";
import SectionWrapper from "../../../components/general/section-wrapper/section-wrapper";
import Title from "../../../components/general/title/title";
import { routeSpecs } from "../../../consts/consts-and-names";
import texts from "../../../consts/text.json";
import PriceElement from "./price-element/price-element";
import styles from "./price-list.module.scss";

const PriceListPage: FC = () => {
  const imageNames = images.imageNames;
  const squares = texts.prices_page.squares;

  const createSquares = () => {
    const list = [];
    for (let i = 0; i < squares.length; i++) {
      list.push(
        <PriceElement
          key={i}
          className={styles["element"]}
          {...squares[i]}
          image={imageNames[i]}
        />
      );
    }
    return list;
  };

  return (
    <PageWrapper>
      <SectionWrapper className={styles["title-section"]}>
        <Title title={texts.prices_page.title} className={styles["title"]} />
      </SectionWrapper>
      <SectionWrapper>
        <div className={styles["container"]}>{createSquares()}</div>
        <Link to={routeSpecs.contact.path} className={styles["link"]}>
          <div className={styles["button"]}>
            <p>{texts.prices_page.button}</p>
          </div>
        </Link>
      </SectionWrapper>
    </PageWrapper>
  );
};

export default PriceListPage;
