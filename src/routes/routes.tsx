import { RouteObject } from "react-router-dom";
import { routeSpecs } from "../consts/consts-and-names";
import PageContainer from "./page-container";
import AboutMePage from "./pages/about-me/about-me";
import ContactPage from "./pages/contact/contact";
import OferPage from "./pages/ofer/ofer";
import PortfolioPage from "./pages/portfolio/portfolio";
import PriceListPage from "./pages/price-list/price-list";

const routes: RouteObject[] = [
  {
    path: "/",
    element: <PageContainer />,
    children: [
      { index: true, element: <AboutMePage /> },
      { path: routeSpecs.portfolio.path, element: <PortfolioPage /> },
      { path: routeSpecs.ofer.path, element: <OferPage /> },
      { path: routeSpecs.contact.path, element: <ContactPage /> },
      { path: routeSpecs.prices.path, element: <PriceListPage /> },
    ],
  },
];

export default routes;
