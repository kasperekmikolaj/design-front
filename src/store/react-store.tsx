import { configureStore, createSlice } from "@reduxjs/toolkit";

type NavigationSliceType = { showMobile: boolean };

const navigationSlice = createSlice({
  name: "nav",
  initialState: { showMobile: false } as NavigationSliceType,
  reducers: {
    changeMobileNavVisibility: (state) => {
      state.showMobile = !state.showMobile;
    },
  },
});

const store = configureStore({ reducer: { nav: navigationSlice.reducer } });

export const navigationActions = navigationSlice.actions;
export type RootStoreState = ReturnType<typeof store.getState>;
export default store;
